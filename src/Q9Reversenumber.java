import java.util.Scanner;

public class Q9Reversenumber {
	//Write a method to implement reverse k numbers first, then next 
	//k numbers and then next k numbers based on the user input.
	
	public static void reverse(int[] a,int n,int k) {
		for(int i=0;i<n;i+=k){
			int left=i;
			int right=Math.min(i+k-1,n-1);
			int temp;
			//reverse the subarray[left,right]
			while(left<right) {
				temp=a[left];
				a[left]=a[right];
				a[right]=temp;
				left+=1;
				right-=1;
			}

		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a= {3,2,4,7,0,3,1,5,8,4};
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number upto which you want to reverse");
		int k=sc.nextInt();
		int n=a.length;
		reverse(a,n,k);
		for(int i=0;i<n;i++) {
			System.out.print(a[i]+ " ");
		}

	}

}
