import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Q8DrinkEnrty {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Drink d1=new Drink("Pepsi",3);
		Drink d2=new Drink("Coke",5);
		Drink d3=new Drink("Sprite",7);
		Drink d4=new Drink("Fanta",4);
		Drink d5=new Drink("Lemonade",2);

		ArrayList<Drink> dlist=new ArrayList<Drink>();
		dlist.add(d1);
		dlist.add(d2);
		dlist.add(d3);
		dlist.add(d4);
		dlist.add(d5);

		/*
		 * Java program to store n number of drinktype with quantity.
		 * If user wants a drink, user should enter drinktype.
		 * If drink quantity is greater than one then request 
		 * should be served and decrease the quantity by one 
		 * else no availability message should be printed on screen.
		 */
		System.out.println("Available drinks: "+dlist);
		Scanner sc=new Scanner(System.in);
		System.out.println("Please enter drink type or type exit:");
		String type; 

		while ((type = sc.nextLine()) != "exit")
		{
			if(type.equalsIgnoreCase("exit"))
			{
				System.out.println("Exiting");
				break;
			}
			else
			{
				Iterator it=dlist.iterator();
				boolean isDrinkConsumed = false;
				while(it.hasNext()) {
					Drink dr = (Drink) it.next();
					if (dr.getdrinkType().equalsIgnoreCase(type))
					{	
						isDrinkConsumed = true;
						if (dr.consumeQty() == 0)
						{
							dlist.remove(dr);
						}
						System.out.println(dr.getdrinkType() + " " + dr.getdrinkQty());
						break;
					}					
					//System.out.println(dr);
				}

				if (Boolean.compare(isDrinkConsumed, false) == 0)
				{
					System.out.println("No available drinks to consume");
				}
				System.out.println("Available drinks: "+dlist);
				System.out.println("Enter drink type or type exit");

			}
		}
	}

}
