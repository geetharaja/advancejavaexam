import java.util.Scanner;
import java.util.Vector;

public class Q6VectorInsert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of times you want to insert the element Z");
		int n=sc.nextInt();
		//creating an empty vector
		Vector<Character> vect=new Vector<Character>();
		
		//use add() methods to add characters in a vector
		vect.add('t');
		vect.add('e');
		vect.add('k');
		vect.add('a');
		vect.add('r');
		vect.add('c');
		vect.add('h');
		System.out.println("The string is: "+vect);
		
		//inserting the character 'Z' for "N" no of times.
		for(int i=0;i<n;i++) {
		vect.insertElementAt('Z', 3);
		}
		System.out.println("After insertion: "+vect);
		
		//Deleting the 'n' characters from 6th position
		for(int i=0;i<n;i++) {
		vect.removeElementAt(6);
		}
		
		System.out.println("After deletion: "+vect);
	}

}
