import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Q5CountNoOfCoins {
	/*Method that returns the smallest possible number of coins for the given input
	 *by finding total  number of dollar coins,quarter coins,dimes,nickels and cents
	 * Example 288=2+3+1+3=9 coins
	 */
	public static void getPossibleCount(long val) {
		
		if(val<0) {
			System.out.println("Enter the valid amount");
		}
		long doll=0,quar=0,dime=0,nick=0,cent=0;
		long total=val;		
		long totalCoins = 0;	

		if (total >= 100)
		{			
			doll += total / 100;
			total = total % 100;
		}
		if (total >= 25)
		{
			quar += total / 25;
			total = total % 25;
		}
		if (total >=10 ) {
			dime += total / 10;
			total = total % 10;
		}
		if(total>=5) {
			nick +=total/5;
			total=total%5;
		}
		cent=total;
		totalCoins =doll+quar+dime+nick+cent;
		
		System.out.println("Dollar: "+doll);
		System.out.println("Quarter: "+quar);
		System.out.println("Dime: "+dime);
		System.out.println("Nickel: "+nick);
		System.out.println("Cents: "+cent);
		System.out.println("Smallest Possible number of coins: "+totalCoins);


	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long amount;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the amount");
		amount=sc.nextInt();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"); 
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now));
		getPossibleCount(amount);
		now = LocalDateTime.now();  
		System.out.println(dtf.format(now));
	}

}
