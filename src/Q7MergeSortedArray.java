
public class Q7MergeSortedArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array1= new int[10];
		array1[0]=1;
		array1[1]=2;
		array1[2]=4;
		array1[3]=6;
		array1[4]=9;
		array1[5]=10;
		int[] array2= {3,5,7,8};
		int n=6;
		int m=array2.length;
		int i=n-1;
		int j=m-1;
		int last=n+m-1;
		while(j>=0) {
			if(i>=0&&array1[i]>array2[j]) {
				array1[last]=array1[i];
				i--;
			}else {
				array1[last]=array2[j];
				j--;
			}
			last--;
		}
		System.out.println("Array1 after merging Array2 in sorted order");
		for(int k=0;k<array1.length;k++) {
			System.out.print(array1[k]+ " ");
		}
	}

}
