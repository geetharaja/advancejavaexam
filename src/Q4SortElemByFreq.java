import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Q4SortElemByFreq {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		//Declare and initialize an array
		int[] array= {2,5,3,8,7,2,5,2,3};
		
		Map<Integer,Integer> mp=new HashMap<Integer,Integer>();
		ArrayList<Integer> OpArray=new ArrayList<Integer>();
		
		//Assign elements and their count in the list and the map
		for (int current : array) { 
            int count=mp.getOrDefault(current,0);
            mp.put(current, count + 1); 
            OpArray.add(current); 
        } 
		
		//compare the map by value
		SortEleComp comp=new SortEleComp(mp);
		
		//sort the map using collections class
		Collections.sort(OpArray, comp);
		
		//Final output
		System.out.println("The final output is :");

		for(Integer i:OpArray) {
			System.out.print(i+ " ");
		}
		
	}

}
