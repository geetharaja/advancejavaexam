import java.util.TreeMap;
import java.util.Map;
import java.util.Set;

public class Q2TreemapSEntry {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s1=new Student(1,"Priya");
		Student s2=new Student(3,"Rohit");
		Student s3=new Student(2,"Vinod");
		Student s4=new Student(5,"Hasita");
		Student s5=new Student(7,"Sam");
		
		//key is Rollno, value is student object
		TreeMap<Integer,Student> tmap=new TreeMap<Integer,Student>();
		
		tmap.put(10,s1);
		tmap.put(5,s2);
		tmap.put(20,s3);
		tmap.put(15,s4);
		tmap.put(25,s5);
		System.out.println("Treemap: "+tmap+ "\n");
		
		Set<Map.Entry<Integer,Student>> set=tmap.entrySet();
		
		System.out.println("Set: "+set+ "\n");
		
		System.out.println("----------------");
		System.out.println("Key"+" | "+ "Value");
		System.out.println("-----------------");
		
		for(Map.Entry<Integer,Student> entry:set) {
			int rollno=entry.getKey();
			Student stu=entry.getValue();
			System.out.println(rollno+ " | "+stu);
		}
	}

}
