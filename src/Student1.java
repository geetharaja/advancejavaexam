import java.util.Comparator;

public class Student1 {
	private int regno;
	private String name;
	private int marks;
	
	public Student1(int rno,String name,int marks) {
		this.regno=rno;
		this.name=name;
		this.marks=marks;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks=marks;
	}
	public int getRegNo() {
		return regno;
	}
	public void setRegNo(int rno) {
		this.regno=rno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	
	public String toString() {
		return "Student name: "+name+ " Student Regno: "+regno +"Marks :"+marks;
	}
	//@Override
//	public int compare(Student1 o1, Student1 o2) {
//		// TODO Auto-generated method stub
//		if(o1.getMarks()>o2.getMarks())
//			return 1;
//		else
//			return -1;
//	}
}
