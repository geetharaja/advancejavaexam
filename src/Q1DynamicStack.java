
public class Q1DynamicStack {
	private int Ssize;
	private int[] stackArr;
	private int top;
	
	public Q1DynamicStack(int size) {//constructor to create stack with size
		this.Ssize=size;
		this.stackArr=new int[size];
		this.top=-1;
	}
	
	//Push method to add new entry to top of the stack
	public void push(int entry) throws Exception {
		if(this.isStackFull()) {
			System.out.println("Stack is full,Increasing the capacity dynamically");
			this.increaseStackCapacity();
		}
		System.out.println("Adding element: "+entry);
		this.stackArr[++top]=entry;
		
	}
	
	public int pop() throws Exception{
		if(this.isStackEmpty()) {
			throw new Exception("Stack is Empty. Cannot remove the element");
		}
		int entry=this.stackArr[top--];
		System.out.println("Removed element: "+entry);
		return entry;
	}
	
	public boolean isStackFull() {
		return (top==Ssize-1);
	}
	
	public boolean isStackEmpty() {
		return (top==-1);
	}
	
	public long peek() {
		return stackArr[top];
	}
	private void increaseStackCapacity() {
		int[] newStack=new int[this.Ssize*2];
		for(int i=0;i<Ssize;i++) {
			newStack[i]=this.stackArr[i];
		}
		this.stackArr=newStack;
		this.Ssize=this.Ssize*2;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Q1DynamicStack ds=new Q1DynamicStack(3);
		for(int i=1;i<10;i++) {
			try {
				ds.push(i);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Removing the elements using pop operation");
		for(int i=1;i<4;i++) {
			try {
				ds.pop();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
