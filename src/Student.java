
public class Student {
	private int regno;
	private String name;
	
	public Student(int rno,String name) {
		this.regno=rno;
		this.name=name;
	}
	public int getRegNo() {
		return regno;
	}
	public void setRegNo(int rno) {
		this.regno=rno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	
	public String toString() {
		return "Student name: "+name+ " Student Regno: "+regno;
	}
}
