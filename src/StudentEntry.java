import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;

public class StudentEntry {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student1 s1=new Student1(1,"Ram",98);
		Student1 s2=new Student1(2,"Tom",87);
		Student1 s3=new Student1(3,"Nancy",75);
		Student1 s4=new Student1(4,"Kavya",98);
		Student1 s5=new Student1(5,"Diya",99);

		ArrayList<Student1> slist=new ArrayList<Student1>();
		slist.add(s1);
		slist.add(s2);
		slist.add(s3);
		slist.add(s4);
		slist.add(s5);
		Collections.sort(slist,new StudComp());
		int rank = 0;
		int lastStudentMarks = -1;
		for(Student1 st:slist) {
			if (lastStudentMarks != st.getMarks())
			{
				rank++;
			}
			lastStudentMarks = st.getMarks();
			System.out.println(st);
			System.out.println("Rank: "+rank);
		}
	}

}
