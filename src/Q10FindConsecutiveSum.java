import java.util.Scanner;

public class Q10FindConsecutiveSum {
	public static void printConsecutive(int val) {
		int start=1;
		int end=(val+1)/2;

		//Repeat the loop from bottom to half
		while(start<=end) {
			//Check if there any sequence exists from bottom to
			//top which adds upto given sum
			int sum=0;
			for(int i=start;i<=end;i++) {
				sum=sum+i;

				//if sum =val, this means consecutive sequence exists

				if(sum==val) {
					System.out.println();
					for(int j=start;j<=i;j++) {
						System.out.print(j+ "+");
					}
					System.out.println("is equal to sum: "+val);
					break;
				}
				if(sum>val) {
					// if sum exceeds val, 
					//then it cannot exist in the consecutive sequence
					break;
				}
			}
			sum=0;
			start++;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;
		Scanner in=new Scanner(System.in);
		System.out.println("Enter a number to find sum of all consecutive numbers");
		n=in.nextInt();
		System.out.print("Possible consecutive number combinations");
		printConsecutive(n);
	}

}
