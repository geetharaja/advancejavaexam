import java.util.Comparator;
import java.util.Map;

public class SortEleComp implements Comparator<Integer>{

	private final Map<Integer, Integer> freqMp; 

	// Assign the specified map 
	SortEleComp(Map<Integer, Integer> tFreqMp) 
	{ 
		this.freqMp = tFreqMp; 
	} 

	//Compare the values

	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		int freqcomp=freqMp.get(o2).compareTo(freqMp.get(o1));
		//System.out.println(freqMp.get(o2)+ " " + freqMp.get(o1) + " " + freqcomp);
		int valuecomp=o1.compareTo(o2);		
		//	System.out.println(o1 + " " + o2 + " " + valuecomp);
		if(freqcomp==0) {
			return valuecomp;
		}
		//else if(freqcomp == -1) {
		//	return valuecomp;
		//}
		else
			return freqcomp;

	}

}
