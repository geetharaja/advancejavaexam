
public class Drink {
 private String drinkType;
 private int drinkQty;
 
 public Drink(String dtype,int dqty) {
	 this.drinkType=dtype;
	 this.drinkQty=dqty;
 }
 public String getdrinkType() {
	 return drinkType;
	 
 }
 public void setdrinkType(String dtype) {
	 this.drinkType=dtype;
 }
 public int getdrinkQty() {
	 return drinkQty;
 }
 public int consumeQty() {
	 drinkQty--;
	 return drinkQty;
 }
 public void setdrinkQty(int dqty) {
	 this.drinkQty=dqty;
 }
 public String toString() {
	 return "Type:" +drinkType+ " Quantity: "+drinkQty;
 }
}
